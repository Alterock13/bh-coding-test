import {
  BrowserModule
} from '@angular/platform-browser';
import {
  NgModule
} from '@angular/core';

import {
  AppRoutingModule
} from './app-routing.module';
import {
  AppComponent
} from './app.component';
import {
  LoginComponent
} from './login/login.component';
import {
  MatToolbarModule
} from '@angular/material/toolbar';
import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import {
  MatInputModule
} from '@angular/material/input';
import {
  MatButtonModule
} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import {
  ToolbarComponent
} from './toolbar/toolbar.component';
import {
  FormsModule
} from '@angular/forms';
import {
  DashboardComponent
} from './dashboard/dashboard.component';
import {
  HttpClientModule
} from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { HighchartsChartModule } from 'highcharts-angular';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ToolbarComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    HttpClientModule,
    HighchartsChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Injectable } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { retryWhen, delay } from 'rxjs/operators';
  
@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private host = 'ws://localhost:4000/updates';
  private isConnected = false;

  private ws = webSocket({
    url: this.host,
    openObserver: {
      next: () => this.isConnected = true
    },
    closeObserver: {
      next: () => this.isConnected = false
    }
  });

  constructor() {
    this.ws
      .pipe(
        retryWhen(e => {
          delay(400);
          return e;
        })
      ).subscribe();

    console.log('Connection status: ' + this.isConnected);
  }

  public sendMessage(msg: string) {
    this.ws.next(msg);
  }

  public onReceiveMessage(){
    return this.ws.asObservable();
  }

  }
import { Component, OnInit } from '@angular/core';
import { DataShareService } from '../data-share.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usernameField: string;
  
  constructor(private service: DataShareService, private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('username')){
      this.router.navigate(['dashboard']);
    }
  }

  usernameSubmit(){
    if(this.usernameField.length > 0){
      localStorage.setItem('username', this.usernameField);
      this.service.changeData(this.usernameField)
      this.router.navigate(['dashboard']);
    }
  }

}

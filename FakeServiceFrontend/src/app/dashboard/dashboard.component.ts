import {
  Component,
  OnInit
} from '@angular/core';
import * as Chart from 'chart.js'
import {
  ApiclientService
} from '../apiclient.service';
import {
  forkJoin,
} from 'rxjs';
import 'hammerjs'
import "chartjs-plugin-zoom";
import {
  Router
} from '@angular/router';
import {
  DataShareService
} from '../data-share.service';
import * as Highcharts from 'highcharts';
import {
  WebSocketService
} from '../websocket.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  chart: Chart;

  // highcharts
  Highcharts: typeof Highcharts;
  updateFlag: boolean = false;

  chartOptions: Highcharts.Options = {
    chart: {
      backgroundColor: "#262626",
      zoomType: 'x'
    },
    xAxis: {
      type: 'datetime',
      labels: {
        formatter: function () {
          return Highcharts.dateFormat('%e %b %y - %I:%M:%S %p', this.value);
        }
      },
      tickInterval: 1000 * 60 * 10, // Ticks every 10 minutes
    },
    plotOptions: {
      series: {
        turboThreshold: 0
      }
    },
    title: {
      text: 'Data',
      style: {
        "color": "#FFFFFF",
        "fontSize": "18px"
      }
    },
    tooltip: {
      formatter: function () {
        let returnString = `Date : ${Highcharts.dateFormat('%e %b %y - %I:%M:%S %p', this.x)}<br/>Value : ${this.y}`
        if (this.point['author']) {
          returnString = returnString.concat(`<br/>Author: ${this.point['author']}`)
        }
        if (this.point['dateModified']) {
          returnString = returnString.concat(`<br/>Date Modified: ${Highcharts.dateFormat('%e %b %y - %I:%M:%S %p', new Date(this.point['dateModified']).getTime())}`)
        }
        return returnString;
      }
    },
    legend: {
      itemStyle: {
        color: "#FFFFFF"
      }
    },
    credits: {
      enabled: false
    },
    series: [{
        name: "Filtered data",
        data: [],
        type: 'line',
        color: 'rgba(158, 162, 235, 1)',
        states: {
          inactive: {
            opacity: 1
          }
        },
      },
      {
        name: "Raw data",
        data: [],
        type: 'line',
        color: 'rgba(54, 162, 235, 1)',
        states: {
          inactive: {
            opacity: 1
          }
        },
        allowPointSelect: true,
        point: {
          events: {
            select: this.modifyData.bind(this)
          }
        }
      },
    ]
  };

  author: string;
  authorChangedTimeout;
  authorMap: Map < string,
  Number[] > = new Map();

  chosenDate: Date = new Date();

  constructor(private apiClient: ApiclientService, private router: Router, private service: DataShareService, private websocket: WebSocketService) {}

  ngOnInit(): void {
    // Retrieve the current author
    this.service.data$.subscribe(res => {
      this.author = res;
    });
    this.author = localStorage.getItem("username");
    // If we try to access /dashboard without a username, redirect to /login
    if (!this.author) {
      this.router.navigate(['login']);
    }
    this.Highcharts = Highcharts;
    // Set up a current date for api calls
    this.chosenDate = new Date();
    this.chosenDate.setHours(0, 0, 0, 0);
  }

  ngAfterViewInit() {
    this.chart = Highcharts.chart('chart', this.chartOptions);

    this.websocket.onReceiveMessage().subscribe(data => {
      // Update with new points in real time only if we selected today's date
      if (this.chosenDate.getDate() == new Date().getDate() && this.chosenDate.getMonth() == new Date().getMonth() && this.chosenDate.getFullYear() == new Date().getFullYear()) {
        this.onWebsocketReceive(data['event']);
      }
    });;
    // Initial Update
    this.updateApiValues();
  }

  // Retrieve the raw and filtered data from the API
  updateApiValues() {
    return new Promise((resolve, reject) => {
      const apiGetRaw = this.apiClient.getRaw(this.chosenDate);
      const apiGetFitlered = this.apiClient.getFiltered(this.chosenDate);

      forkJoin(apiGetRaw, apiGetFitlered).subscribe(([rawResponse, filteredResponse]: [any[], any[]]) => {
        this.updateChart(rawResponse, filteredResponse)
        resolve()
      })
    });

  }

  // Updates the data and change color for points on the chart
  updateChart(rawResponse, filteredResponse) {
    // Raw Data
    this.chart.series[1].setData(rawResponse.map((d, index) => {
      let object = {
        x: new Date(d.createdOn).getTime(),
        y: d.value,
        author: d.author,
        documentID: d._id
      }
      if (d.author) {
        let authourLookup = this.authorMap.get(d.author)
        if (authourLookup) {
          authourLookup.push(index)
        } else {
          this.authorMap.set(d.author, [index])
        }
      }
      if (this.author == d.author) {
        object['color'] = 'rgba(255, 0, 0, 1)'
        object['marker'] = {
          enabled: true,
          lineColor: 'rgba(255, 0, 0, 1)'
        }
      } else {
        object['color'] = undefined
        object['marker'] = {
          enabled: false,
          lineColor: undefined
        }
      }
      return object;
    }));

    // Filtered Data
    this.chart.series[0].setData(filteredResponse.map(d => {
      return ({
        x: new Date(d.createdOn).getTime(),
        y: d.value,
        author: d.author,
        documentID: d._id
      })
    }));
  }

  // When the user clicks on a point, change the raw data with an API call 
  modifyData(event) {
    this.apiClient.putChange(event.target.documentID, localStorage.getItem('username')).subscribe((data: any[]) => {})
  }

  onWebsocketReceive(data) {
    // insert
    if (data.operation == "insert") {
      const newOjbect = {
        x: new Date(data.data.createdOn).getTime(),
        y: data.data.value,
        documentID: data.data._id
      }
      if (data.type == 'raw') {
        newOjbect['author'] = data.data.data;
        this.chart.series[1].addPoint(newOjbect)
      } else if (data.type == 'filtered') {
        this.chart.series[0].addPoint(newOjbect)
      }
      // Update
    } else if (data.operation == "update") {
      var index = this.chart.series[1].data.findIndex(d => {
        return d.documentID == data._id;
      })
      this.chart.series[1].data[index].update({
        author: data.changes.author,
        y: data.changes.value,
        originalValue: data.changes.originalValue,
        dateModified: data.changes.dateModified

      })
      this.colorPoint(data.changes.author, index)
      // Unselect the current point
      this.chart.series[1].data[index].update({
        selected: false
      })
    }
  }

  dateSelect(event) {
    this.chosenDate = event.value;
    this.chosenDate.setHours(0, 0, 0, 0);
    this.updateApiValues()
  }

  colorPoint(autr, index) {
    if (this.author == autr) {
      this.chart.series[1].data[index].update({
        color: 'rgba(255, 0, 0, 1)',
        marker: {
          enabled: true,
          lineColor: 'rgba(255, 0, 0, 1)'
        }
      }, false) // The false flags if for not redrawing immediately
    } else {
      this.chart.series[1].data[index].update({
        color: undefined,
        marker: {
          enabled: false,
          lineColor: undefined
        }
      }, false)
    }
  }

  authorChanged(value) {
    this.author = value;
    if (this.authorChangedTimeout) {
      clearTimeout(this.authorChangedTimeout);
    }
    this.authorChangedTimeout = setTimeout(() => {
      // We select the correct point to display in red
      this.chart.series[1].data.forEach((point, index) => {
        this.colorPoint(point.author, index);
      });
      this.chart.redraw(); // We only redraw once at the end for better performances
    }, 1000);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiclientService {

  private SERVER_URL = "http://localhost:4000";
	constructor(private httpClient: HttpClient) { }

	public getRaw(date){
    let endDay = new Date(date);
    endDay.setHours(23,59,59,999);
		return this.httpClient.get(`${this.SERVER_URL}/raw?start=${date.toISOString()}&end=${endDay.toISOString()}`);  
	}  

	public getFiltered(date){
    let endDay = new Date(date);
    endDay.setHours(23,59,59,999);  
		return this.httpClient.get(`${this.SERVER_URL}/filtered?start=${date.toISOString()}&end=${endDay.toISOString()}`);  
  }  
  
  public putChange(id, author){
    return this.httpClient.put(`${this.SERVER_URL}/modify`, {'_id': id, 'author': author})
  }

}

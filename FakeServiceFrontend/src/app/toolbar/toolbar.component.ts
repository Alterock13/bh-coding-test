import {
  Component,
  OnInit
} from '@angular/core';
import {
  DataShareService
} from '../data-share.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  username: string;

  constructor(private service: DataShareService) {}

  ngOnInit(): void {
    this.service.data$.subscribe(res => this.username = res)
    this.username = localStorage.getItem("username");
    let that = this;
    // document.getElementById("username").addEventListener("input", function () {
    //   that.service.changeData(that.username);
    //   localStorage.setItem('username', that.username);
    // }, false);
  }

  checkUsername(username) {
    let pattern = new RegExp(/^[a-zA-Z0-9]+$/);
    if (pattern.test(username) && username.length > 0) {
      this.username = username;
      this.service.changeData(this.username);
      localStorage.setItem('username', this.username);
    } else {
      document.getElementById("username").innerHTML = this.username
    }
  }

}

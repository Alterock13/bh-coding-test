const aedes = require('aedes')({
    concurrency: 0,
    id: 'broker'
})

const server = require('net').createServer(aedes.handle)
const port = 1883

// On subscription of topics
aedes.on('subscribe', function (subscriptions, client) {
    console.log(`MQTT client \x1b[31m ${client ? client.id : client} \x1b[0m  subscribed to topics: ${subscriptions.map(s => s.topic).join('\n')} from broker ${aedes.id}`)
})

// When a message is published
aedes.on('publish', async function (packet, client) {
    console.log(`MQTT client \x1b[31m ${client ? client.id : aedes.id} \x1b[0m has published ${packet.payload.toString()} on ${packet.topic} to broker ${aedes.id}`)
})

server.listen(port, function () {
    // We generate an array for the Fibonacci sequence for values before 100
    console.log('server started and listening on port ', port)
})
var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://broker:1883', {
  clientId: "filtering"
})

// Instanciate the fibonnaci sequence up until 89
const fibonacciSequence = fibonacci(11);

/**
 * Generate the Fibonnaci sequence up until a certain point
 * @param {Number} n corresponds to F(n)
 */
function fibonacci(n) {
  if (n == 0) return [0];
  if (n == 1) return [0, 1];
  const arr = fibonacci(n - 1);
  return [...arr, arr[n - 1] + arr[n - 2]];
}

/**
 * For a given value, find the closest number from the Fibonnaci sequence, or 100 for value>100
 * @param {Number} value Value to filter
 */
function filtering(value) {
  if (value >= 100) return 100;
  let closest = fibonacciSequence.reduce((a, b) => {
    return Math.abs(b - value) <= Math.abs(a - value) ? b : a;
  });
  return closest
}

/**
 * Callback used when a MQTT message is received on topic /probe
 */
client.on('message', function (topic, message) {
  let filtered = filtering(Number(message));
  client.publish('filtered', filtered.toString())
});

client.subscribe("probe")
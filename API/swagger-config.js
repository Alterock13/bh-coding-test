module.exports = {
    swaggerDefinition: {
        info: {
            version: "1.0.0",
            title: "BH coding test API",
            description: "API Information",
            contact: {
                name: "Arnaud Pierret"
            },
            servers: ["http://localhost:4000"]
        }
    },
    apis: ["server.js"]
};
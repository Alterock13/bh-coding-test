const express = require('express');

const utils = require('./utils')

const server = express();
server.use(express.json())
const cors = require('cors');

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerConf = require('./swagger-config');


var mongoose = require('mongoose');
var mongoDB = 'mongodb://mongodb/';
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: false,
    useCreateIndex: true,
    replicaSet: 'rs0',
    dbName: 'my_database'
}, (err, db) => {
    if (err) {
        console.log('Unable to connect to the server. Stated error was:', err);
        // return process.exit(1);
    } else {
        console.log(`Successful connection to MongoDB at ${mongoDB}`);
    };
});

// Used to signal the websocket server that new data is available from the database
const {
    EventEmitter
} = require('events');
const updatesEventEmitter = new EventEmitter();

const dataRawModel = mongoose.model("dataRaw", {
    value: Number,
    createdOn: Date,
    originalValue: Number,
    author: String,
    dateModified: Date
});

dataRawModel.watch().on('change', data => {
    if (data.operationType == "insert") {
        updatesEventEmitter.emit("updateEvent", {
            operation: 'insert',
            type: 'raw',
            data: {
                _id: data.fullDocument._id,
                value: data.fullDocument.value,
                createdOn: data.fullDocument.createdOn
            }
        });
    } else if (data.operationType == "update") {
        updatesEventEmitter.emit("updateEvent", {
            operation: 'update',
            type: 'raw',
            _id: data.documentKey._id,
            changes: data.updateDescription.updatedFields
        });
    }
});



const dataFilteredModel = mongoose.model("dataFiltered", {
    value: Number,
    createdOn: Date
});

dataFilteredModel.watch().on('change', data => {
    if (data.operationType == "insert") {
        updatesEventEmitter.emit("updateEvent", {
            operation: 'insert',
            type: 'filtered',
            data: {
                _id: data.fullDocument._id,
                value: data.fullDocument.value,
                createdOn: data.fullDocument.createdOn
            }
        });
    } else if (data.operationType == "update") {
        updatesEventEmitter.emit("updateEvent", {
            operation: 'update',
            type: 'filtered',
            _id: data.documentKey._id,
            changes: data.updateDescription.updatedFields
        });
    }
});

// Add headers
server.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
    next();
});
server.use(cors());

const swaggerOptions = {
    swaggerDefinition: swaggerConf,
    apis: ["server.js"]
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));


async function retrieveDbValues(model, filter, start, end, res) {
    const dateStart = start;
    const dateEnd = end;
    let convertedStartDate;
    let convertedEndDate;
    if (dateStart || dateEnd) {
        if (dateStart) {
            try {
                convertedStartDate = new Date(dateStart)
                if(!convertedStartDate.getTime()) throw Error('Not a start valid date');
            } catch (error) {
                console.log("Start is not a correctly formatted date")
                return res.status(422).end();
            }
        }
        if (dateEnd) {
            try {
                convertedEndDate = new Date(dateEnd)
                if(!convertedEndDate.getTime()) throw Error('Not a end valid date');
            } catch (error) {
                console.log("End is not a correctly formatted date")
                return res.status(422).end();
            }
        }
        if (!dateStart) {
            filter.createdOn = {
                "$lt": dateEnd
            }
        } else if (!dateEnd) {
            filter.createdOn = {
                "$gte": dateStart,
            }
        } else if (dateStart < dateEnd) {
            filter.createdOn = {
                "$gte": dateStart,
                "$lt": dateEnd
            }
        } else {
            console.log("Start date is after End date")
            return res.status(422).end();
        }
    }
    const all = await model.find(filter, {
        __v: 0
    });
    return res.json(
        all
    ).end();
}

/**
 * @swagger
 * /raw:
 *  get:
 *    description: Use to request all raw data
 *    parameters:
 *        - in: query
 *          name: start
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date from which the data will be retrieved
 *        - in: query
 *          name: end
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date until which the data will be retrieved
 *    produces:
 *       - application/json
 *    responses:
 *      '200':
 *        description: Raw data is retrieved
 *        schema:
 *           type: array
 *           items:
 *               type: object
 *               properties:
 *                _id:
 *                    type: string
 *                value:
 *                    type: number
 *                createdOn:
 *                    type: string
 *                    format: date-time
 *                author:
 *                    type: string
 *                dateModified:
 *                    type: string
 *                    format: date-time
 *                originalValue:
 *                    type: number
 *      '304':
 *        description: No new data was available in the database, cache is used     
 */
server.get("/raw", (req, res) => {
    retrieveDbValues(dataRawModel, {}, req.query.start, req.query.end, res)
});

/**
 * @swagger
 * /filtered:
 *  get:
 *    description: Use to request all filtered data
 *    parameters:
 *        - in: query
 *          name: start
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date from which the data will be retrieved
 *        - in: query
 *          name: end
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date until which the data will be retrieved
 *    produces:
 *       - application/json
 *    responses:
 *      '200':
 *        description: Filtered data is retrieved
 *        schema:
 *           type: array
 *           items:
 *               type: object
 *               properties:
 *                _id:
 *                    type: string
 *                value:
 *                    type: number
 *                createdOn:
 *                    type: string
 *                    format: date-time
 *      '304':
 *        description: No new data was available in the database      
 */
server.get("/filtered", (req, res) => {
    retrieveDbValues(dataFilteredModel, {}, req.query.start, req.query.end, res)
});

/**
 * @swagger
 * /modified:
 *  get:
 *    description: Use to request all modified data
 *    parameters:
 *        - in: query
 *          name: start
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date from which the data will be retrieved
 *        - in: query
 *          name: end
 *          schema:
 *              type: string
 *              format: date-time
 *          description: an ISO formatted date until which the data will be retrieved
 *    produces:
 *       - application/json
 *    responses:
 *      '200':
 *        description: Modified data is retrieved
 *        schema:
 *           type: array
 *           items:
 *               type: object
 *               properties:
 *                _id:
 *                    type: string
 *                value:
 *                    type: number
 *                createdOn:
 *                    type: string
 *                    format: date-time
 *                author:
 *                    type: string
 *                dateModified:
 *                    type: string
 *                    format: date-time
 *                originalValue:
 *                    type: number
 *      '304':
 *        description: No new data was available in the database      
 */
server.get("/modified", (req, res) => {
    const filter = {
        dateModified: {
            $exists: true,
            $ne: null
        }
    };
    if (req.query.name) filter.author = req.query.name;
    retrieveDbValues(dataRawModel, filter, req.query.start, req.query.end, res)
});

/**
 * @swagger
 * /modify:
 *   put:
 *     description: Allows to change raw data value to be equal to filtered data
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: _id
 *         description: Document id from the Mongodb database
 *         in: formData
 *         required: true
 *         type: string
 *       - name: author
 *         description: Name of the person changing the data point
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Document that has been modified
 *         schema:
 *           type: object
 *           properties:
 *            _id:
 *                type: string
 *            value:
 *                type: number
 *            createdOn:
 *                type: string
 *                format: date-time
 *            author:
 *                type: string
 *            dateModified:
 *                type: string
 *                format: date-time
 *            originalValue:
 *                type: number
 */
server.put("/modify", async (req, res) => {
    if (!req.body._id) return res.status(404).end();; // In case no id is given
    if (!req.body.author) return res.status(422).end();; // In case no id is given

    let document = await dataRawModel.findById(req.body._id);

    if (document.author) return res.status(304).end(); // If the point has already been modified, do not modify again

    document.originalValue = document.value;
    document.value = utils.filtering(document.value);
    document.author = req.body.author;
    document.dateModified = new Date();

    document.save((err, result) => {
        if (err) {
            console.error('Could not modify the value');
            return res.status(500).end()
        } else {
            res.send(document)
        }
    });
});

module.exports = {
    server: server,
    updatesEventEmitter: updatesEventEmitter
};
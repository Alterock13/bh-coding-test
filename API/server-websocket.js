'use strict';

let WSServer = require('ws').Server;
let server = require('http').createServer();
let app = require('./server-http').server;
let updateEvent = require('./server-http').updatesEventEmitter;
const host = '0.0.0.0'
const port = 4000;

// Create web socket server on top of a regular http server
let wss = new WSServer({
    server: server
});

var wsClients = [];

// Mount the HTTP express   server
server.on('request', app);

updateEvent.on('updateEvent', (event) => {
    wsClients.forEach((client) => {
        if (client.readyState == 1) {
            client.send(JSON.stringify({
                event
            }));
        }
    })
});

wss.on('connection', function connection(ws) {
    wsClients.push(ws)
    ws.on('message', function incoming(message) {
        console.log(`received: ${message}`);

        ws.send(JSON.stringify({
            answer: 42
        }));
    });
});


server.listen(port, host, () => {
    console.log(`Server listening at http://${host}:${port}`);
});
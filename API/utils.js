const fibonacciSequence = fibonacci(11);

function fibonacci(n) {
  if (n == 0) return [0];
  if (n == 1) return [0, 1];
  const arr = fibonacci(n - 1);
  return [...arr, arr[n - 1] + arr[n - 2]];
}

function filtering(value) {
  if (value >= 100) return 100;
  let closest = fibonacciSequence.reduce((a, b) => {
    return Math.abs(b - value) <= Math.abs(a - value) ? b : a;
  });
  return closest
}

module.exports.filtering = filtering;
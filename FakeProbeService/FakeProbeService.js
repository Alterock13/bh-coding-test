var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://broker:1883', {
  clientId: "probe"
})

/**
 * Returns a random value between min and max
 * @param {Number} min 
 * @param {Number} max 
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * Math.floor(max)) + min;
}

/**
 * Publish MQTT messages on topic /probe on a regular basis
 * @param {Number} durationMin highest frequency of messages possible
 * @param {Number} durationMax lowest freuqency of messages possible
 */
function sendProbe(durationMin, durationMax) {
  let counter = getRandomInt(durationMin, durationMax);
  const value = getRandomInt(-10, 150).toString()
  console.log(`Publishing value ${value}`)
  client.publish('probe', value)
  setTimeout(sendProbe, counter, durationMin, durationMax);
}

client.on('connect', function () {
  sendProbe(2000, 4000);
})
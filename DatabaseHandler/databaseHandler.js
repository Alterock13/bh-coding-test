var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://broker:1883', {
    clientId: "dbhandler"
})

var mongoose = require('mongoose');
// Connecting to 2 MongoDB instances allows redundancy but also the access to watch on replica sets for notifications on document changes
var mongoDB = 'mongodb://mongodb/';
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: false,
    useCreateIndex  : true,
    replicaSet      : 'rs0',
    dbName : 'my_database'
}, (err, db) => {
    if (err) {
        console.log('Unable to connect to the server. Stated error was:', err);
        return process.exit(1);
    } else {
        console.log('Successful connection !');
    };
});

// Data model for the Mongo database
const dataRawModel = mongoose.model("dataRaw", {
    value: Number,
    createdOn: Date,
    originalValue: Number,
    author: String,
    dateModified: Date,
});

const dataFilteredModel = mongoose.model("dataFiltered", {
    value: Number,
    createdOn: Date
});

client.on('message', async function (topic, message) {
    if (topic == "probe") {
        var rawValueDocument = new dataRawModel({
            value: Number(message),
            createdOn: new Date()
        });
        await rawValueDocument.save();
    } else if (topic == "filtered") {
        var filteredValueDocument = new dataFilteredModel({
            value: Number(message),
            createdOn: new Date()
        });
        await filteredValueDocument.save();
    }

})

client.subscribe("probe")
client.subscribe("filtered")